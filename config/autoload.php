<?php
spl_autoload_register(function ($clazz) {
	require_once
		(dirname(__DIR__) .
		DIRECTORY_SEPARATOR .
		str_replace('\\', DIRECTORY_SEPARATOR, $clazz)
		. '.php');
});