-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2015 at 11:27 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `blogapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `billets`
--

CREATE TABLE IF NOT EXISTS `billets` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `titre` varchar(64) DEFAULT NULL,
  `body` text,
  `cat_id` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `billets`
--

INSERT INTO `billets` (`id`, `titre`, `body`, `cat_id`, `created_at`, `updated_at`) VALUES
(1, 'go sluc, go', 'tout est dans le titre', 1, NULL, NULL),
(2, 'Concert : nolwenn live', 'c''est d''la balle, Ca vaut bien Mick Jagger et Iggy Stooges réunis.\r\nAngus Young doit l''ecouter en boucle...', 3, NULL, NULL),
(3, 'Titanic', 'c''est l''histoire d''un gros bateau qui croise un glaçon', 2, NULL, NULL),
(4, 'Hello darling', 'Hi !', 2, '2014-12-16 09:52:51', '2014-12-16 09:52:51'),
(5, 'fd', 'df', 1, '2014-12-16 09:57:09', '2014-12-16 09:57:09'),
(6, 'fd', 'df', 1, '2014-12-16 10:00:07', '2014-12-16 10:00:07'),
(7, '', '', 0, '2014-12-16 10:02:29', '2014-12-16 10:02:29'),
(8, '', '', 0, '2014-12-16 10:03:09', '2014-12-16 10:03:09'),
(9, '', '', 0, '2014-12-16 10:03:10', '2014-12-16 10:03:10'),
(10, 'df', 'df', 1, '2014-12-16 10:20:43', '2014-12-16 10:20:43'),
(11, 'Hello !', 'Coucou', 4, '2014-12-16 10:37:17', '2014-12-16 10:37:17'),
(12, 'Le verkin du 54', 'Le verkin du 54', 40, '2014-12-19 07:35:27', '2014-12-19 07:35:27'),
(13, 'Bisoutkette', '<h1>Coucou bistoukette</h1>\n', 1, '2014-12-19 07:47:45', '2014-12-19 07:47:45');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `titre` varchar(64) NOT NULL,
  `description` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `titre`, `description`, `created_at`, `updated_at`) VALUES
(1, 'sport', 'tout sur le sport en general', NULL, NULL),
(2, 'cinema', 'tout sur le cinema', NULL, NULL),
(3, 'music', 'toute la music que j''aaiiiimeuh, elle vient de la, elle vient du bluuuuuuzee', NULL, NULL),
(4, 'tele', 'tout sur les programmes tele, les emissions, les series, et vos stars preferes', NULL, NULL),
(40, 'Pierrot la pierre', 'C&#39;est l&#39;histoire de Pierrot la pierre ...', '2014-12-19 07:12:01', '2014-12-19 07:12:01'),
(39, 'fg', 'fg', '2014-12-16 10:48:03', '2014-12-16 10:48:03'),
(36, '', '', '2014-12-16 10:42:20', '2014-12-16 10:42:20'),
(37, '', '', '2014-12-16 10:45:35', '2014-12-16 10:45:35'),
(38, '', '', '2014-12-16 10:46:28', '2014-12-16 10:46:28');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
