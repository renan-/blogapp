<?php

class HttpRequestTest extends PHPUnit_Framework_TestCase {
	private $req1;

	public function setUp() {
		$this->req1 = new \blogapp\test\MockHttpRequest('GET', '/www/php/dumbapp/index.php', '/www/php/dumbapp/all', '', array(), array());
		$this->req2 = new \blogapp\test\MockHttpRequest('GET', '/www/php/dumbapp/index.php', '/www/php/dumbapp/all', '?id=12', array('id' => 12), array());
	}

	public function testGetPathInfo() {
		$this->assertEquals('/all', $this->req1->getPathInfo());
	}

	public function testDispatch1Route() {
		$this->expectOutputString("dummyAction");

		$d = new \generic\dispatch\Dispatcher($this->req1);
		$d->addRouteGet('/all', '\blogapp\test\BasicController', 'dummyAction');

		$d->dispatch();
	}

	public function testDispatchPlusieursRoute() {
		$this->expectOutputString("dummyAction");

		$d = new \generic\dispatch\Dispatcher($this->req1);
		$d->addRouteGet('/all', '\blogapp\test\BasicController', 'dummyAction');
		$d->addRouteGet('/All', '\blogapp\test\BasicController', 'test');
		$d->addRouteGet('/ball', '\blogapp\test\BasicController', 'test1');
		$d->addRouteGet('/allb', '\blogapp\test\BasicController', 'test2');

		$d->dispatch();
	}

	public function testDispatchParamsUrl() {
		$this->expectOutputString("dummyAction 12");

		$d = new \generic\dispatch\Dispatcher($this->req2);
		$d->addRouteGet('/all', '\blogapp\test\BasicController', 'dummyAction');

		$d->dispatch();
	}
}