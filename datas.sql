-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 17, 2015 at 01:49 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `blogapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `billets`
--

CREATE TABLE IF NOT EXISTS `billets` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `titre` varchar(64) DEFAULT NULL,
  `body` text,
  `cat_id` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `billets`
--

INSERT INTO `billets` (`id`, `titre`, `body`, `cat_id`, `created_at`, `updated_at`, `user_id`, `image_id`) VALUES
(1, 'Moi au boulot !', 'Moi lorsque je code depuis 2 heures et que mon code PHP fonctionne du premier coup !', 2, '2015-01-17 00:14:15', '2015-01-17 00:14:15', 2, 2),
(2, 'Moi au boulot 2 !', '##Moi lorsque la NSA vient de m&#39;espionner pendant que j&#39;étais sur Facebook !', 2, '2015-01-17 00:16:25', '2015-01-17 00:16:25', 2, 3),
(3, 'C&#39;est l&#39;histoire d&#39;un mec ...', 'C&#39;est un mec il est dans une rivière, il crie : HELP, HELP !!! Y&#39;a un mec qui passe il dit : au lieu d&#39;apprendre l&#39;Anglais il ferait mieux d&#39;apprendre à nager lui !\r\n\r\nLe roi Dagobert, tu sais comment qu&#39;on l&#39;appelait quand il allait à l&#39;école ? Et bien, le roi Dagobert, il mettait ses vêtements à l&#39;envers. Et les autres ont leur nom sur leur vêtements. Mais lui, il avait pas son nom, il mettait à l&#39;envers, on l&#39;appelait &#34;Pur Coton&#34; !\r\n\r\nC&#39;est le patron qui dit à son employé : &#34;On va vous envoyer au Brésil.&#34; &#34;Oh, le Brésil j&#39;aime pas ça : au Brésil y&#39;a que des putes et des footballeurs !&#34;. Alors le patron dit : &#34;Mais, vous n&#39;ignorez pas que ma femme est Brésilienne !&#34;. Il dit : &#34;Ah bon, et elle joue dans quelle équipe !&#34;.\r\n\r\nTu sais pourquoi les Indiens criaient tout le temps sur leurs chevaux ? Bah, c&#39;est parce qu&#39;on oublie de dire un truc, c&#39;est qu&#39;ils avaient pas de selle, mais ils avaient pas de slip non plus !\r\n\r\nC&#39;est la maîtresse qui fait l&#39;appel à l&#39;école : Bénali... Présent Benyoukou... Présent Benmohamid... Présent Benoïte... Le mec il se lève et il dit : &#34;Non, Benoît !&#34;.\r\n\r\nAlors est ce que vous savez la différence qu&#39;il y a entre un lion qui a pas mangé et un policier ordinaire ? Et bien y&#39;en a un c&#39;est un fauve à jeun (un faux vagin) et l&#39;autre c&#39;est un vrai con !\r\n\r\nC&#39;est Alain Delon qui s&#39;arrête dans une station-service et il demande : &#34;Est-ce que vous avez des chiottes privés ?&#34; &#34;Non, j&#39;ai des chiottes publics&#34; &#34;Oh, tant pis, j&#39;y vais, j&#39;ai trop envie !&#34;. Et Alain Delon il ressort des chiottes, il est couvert de pisse jusqu&#39;aux genoux. Alors le pompiste lui dit : &#34;Mais, vous vous sentez pas pisser !&#34;. Et Alain Delon répond : &#34;Mais, c&#39;est pas moi parce que, quand j&#39;arrive aux urinoirs, tout le monde se retourne et dit &#34;Oh, Alain Delon !&#34;.\r\n\r\nAu bureau, y&#39;a une secrétaire qui sort des chiottes avec un Tampax sur l&#39;oreille, y&#39;a un type qui lui fait remarquer et elle fait &#34;Oh, mon crayon !&#34;.\r\n\r\n', 1, '2015-01-17 00:19:50', '2015-01-17 00:19:50', 1, -1),
(4, 'Au secours le PHP !', 'Obligé de faire cette tête pour q&#39;une personne charitable m&#39;aide a débugger mon code PHP !', 2, '2015-01-17 00:38:44', '2015-01-17 00:38:44', 1, 4),
(5, 'Ruby ?', '#A quand Ruby and rails à l&#39;IUT ?', 2, '2015-01-17 00:41:23', '2015-01-17 00:41:23', 3, 5);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `titre` varchar(64) NOT NULL,
  `description` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `titre`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Humour', 'L&#39;humour à travers les générations !', '2015-01-16 22:24:43', '2015-01-16 22:24:43'),
(2, 'Informatique', 'L&#39;informatique comme vous ne l&#39;avez jamais vu !', '2015-01-16 22:31:26', '2015-01-16 22:31:26');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `filename`) VALUES
(2, '083d1d18a896c00ac5792ac54de73d90.jpg'),
(3, '6b9daabae90f652f9062344fbe10c88b.jpg'),
(4, 'e87ae0a1ca16647430efe98c006c76fd.jpg'),
(5, '3d0bf611de85611459f737689539cdb8.png');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'FHollande', '$2y$12$xh3ds/T19v5N3cFIIT2MYewQIkfeJLbDLSdAsGgjzuCuh7XbHqkBG'),
(2, 'Barack Obama', '$2y$12$tk//TKWTP48fgGItkqVCOuZxKx5qwN91g/n1BHnF9Vg1G86lEKGXO'),
(3, 'anonymous', '$2y$12$KGy5crTAP.vAwWpzO3kqt.tvxOdmFwrTSJlbVT.bv.9oYMjNdVJV6');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
