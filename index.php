<?php
session_start();
require_once('vendor/autoload.php');

//include_once('TestBD.php');

use generic\Helper;
use generic\utils\HttpRequest;
use generic\dispatch\Dispatcher;

Helper::bootEloquent('db.ini');

$req = new HttpRequest();

$d = new Dispatcher($req);


// Blog controller
	// -- GET
$d->addRouteGet('/', '\blogapp\control\BlogController', 'index');
$d->addRouteGet('/blog/billet', '\blogapp\control\BlogController', 'afficherBillet');
$d->addRouteGet('/blog/cat', '\blogapp\control\BlogController', 'messagesCategorie');
$d->addRouteGet('/blog/list', '\blogapp\control\BlogController', 'listerBillets');
$d->addRouteGet('/blog/listcat', '\blogapp\control\BlogController', 'listerCategories');
$d->addRouteGet('/blog/user', '\blogapp\control\BlogController', 'listerBilletsAuteur');

// Admin controller
	// -- GET
$d->addRouteGet('/admin/cat/add', '\blogapp\control\AdminController', 'ajouterCategorie');
$d->addRouteGet('/admin/billet/add', '\blogapp\control\AdminController', 'ajouterMessage');
	// -- POST
$d->addRoutePost('/admin/billet/save', '\blogapp\control\AdminController', 'sauverMessage');
$d->addRoutePost('/admin/cat/save', '\blogapp\control\AdminController', 'sauverCategorie');


// User controller
	// -- GET
$d->addRouteGet('/user/add', '\blogapp\control\UserController', 'ajouterUtilisateur');
$d->addRouteGet('/user/logout', '\blogapp\control\UserController', 'deconnecterUtilisateur');
$d->addRouteGet('/user/signin', '\blogapp\control\UserController', 'connexionUtilisateur');
	// -- POST
$d->addRoutePost('/user/save', '\blogapp\control\UserController', 'sauverUtilisateur');
$d->addRoutePost('/user/login', '\blogapp\control\UserController', 'connecterUtilisateur');

// Let's do it !
$d->dispatch();