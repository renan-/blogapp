<?php
namespace blogapp\view;

use \blogapp\model\Categorie;
use \blogapp\model\Billet;
use \blogapp\model\User;
use \generic\utils\HttpRequest;
use \generic\utils\Authentication;
use \Michelf\Markdown;


/**
 * Vue du blog.
 *
 * @author Renan Strauss (S3A)
 * @author Yann Prono (S3A)
 */
class VueBlog {
	private $elements;

	const LISTE  = 0;
	const DETAIL = 1;
	const HOME   = 2;
	const LISTE_CAT   = 3;

	const SIZE_BODY = 250;

	public function __construct($req, $cat, $elements) {
		$this->httpRequest = $req;
		$this->elements = $elements;
		$this->cat = $cat;
	}

	public function render($title, $dType, $show_subtitle = true) {
		$html = "<!doctype html><html><head><title>Blog</title>";
		$html .= '<link rel="stylesheet" href="'.$this->httpRequest->getRootUri().'/public/css/pure-min.css">';
		$html .= '<link rel="stylesheet" href="'.$this->httpRequest->getRootUri().'/public/css/side-menu.css">';
		$html .= '<link rel="stylesheet" href="'.$this->httpRequest->getRootUri().'/public/css/custom.css">';
		$html .= '<meta name="viewport" content="width=device-width, initial-scale=1">';
		$html .= '<meta charset="utf-8">';
		$html .= "</head><body>";

		$html .= <<<EOD
        <div class="pure-menu pure-menu-open pure-menu-horizontal navbar">
            <ul>
EOD;
        $html .= '<li class="pure-menu-selected"><a href="'.$this->httpRequest->getRootUri().'">Accueil</a></li>';
        if(Authentication::isGuest()) {
        	$html .= '<li><a href="'.$this->httpRequest->getRootUri().'/user/add'.'">S\'enregistrer</a></li>';
        	$html .= '<li><a href="'.$this->httpRequest->getRootUri().'/user/signin'.'">Se connecter</a></li>';
        } else {
		$html .= '<li><a href="'.$this->httpRequest->getRootUri().'/admin/billet/add'.'">Ajouter un billet</a></li>';
        $html .= '<li><a href="'.$this->httpRequest->getRootUri().'/admin/cat/add'.'">Ajouter une catégorie</a></li>';
		$html .= '<li><a href="'.$this->httpRequest->getRootUri().'/blog/user?id='.$_SESSION['profile']['uid'].'">'.$_SESSION['profile']['username'].'</a></li>';
		$html .= '<li><a href="'.$this->httpRequest->getRootUri().'/user/logout'.'">Se déconnecter</a></li>';
        }
        $html .= '</ul></div>';

		$html .= $this->generateCatsBlock();
		$html .= $this->generateLastTicketsBlock();

		$content = "";
		if(isset($_SESSION['info'])) {
			$content = '<p class="pure-flash pure-flash-success">'.$_SESSION['info'].'</p>';
			unset($_SESSION['info']);
		}

		switch($dType) {
			case self::LISTE:
				$content .= $this->generateMany($this->elements);
				break;
			case self::LISTE_CAT:
				$content .= $this->generateManyCat($this->elements);
				break;
			case self::DETAIL:
				if(count($this->elements) > 0) {
					$content .= $this->generateOne($this->elements[0]);
				}
				break;
			default:
		}

		if($show_subtitle) {
			if(count($this->elements) > 0) {
				$subtitle = count($this->elements) . " billet(s)";
			} else {
				$subtitle = "Pas de billet à afficher";
			}
		} else {
			$subtitle = "";
		}

		$html .= <<<EOD
    	<div id="main">
        	<div class="header">
            	<h1>$title</h1>
            	<h2>$subtitle</h2>
            </div>
            <div class="content">
				$content
			</div>
        </div>
EOD;

		$html .= "<script src=\"../public/js/ui.js\"></script></body></html>";

		echo $html;
	}

	private function generateOne($billet) {
		$html = Markdown::defaultTransform($billet->body);
		if($billet->image != null) {
			$html .= '<img class="image" src="'.$this->httpRequest->getRootUri().'/public/img/'.$billet->image->filename.'" alt="Blogapp">';	
		}		
		$html .= '<span class="infos"> Posté par ';
		$html .= '<a href="';
		$html .= ($billet->user == null) ? '#' : ($this->httpRequest->getRootUri() . '/blog/user?id=' . $billet->user->id);
		$html .= '">';
		$html .= ($billet->user == null) ? 'un illustre inconnu': $billet->user->username;
		$html .= '</a>';
		$html .= ' le '.$this->generateDate($billet->updated_at);
		$html .= '</span>';
		return $html;
	}

	/**
	 * Genere la liste des billets.
	 */
	private function generateMany($elements) {

		$html = $this->cat != null ? '<h3 class="desc">'.$this->cat->description.'</h3>' :'';
		/** Pour chaque billet */
		foreach ($elements as $key => $billet) {
			$html .= '<div class="billet">';
			// Enleve le balisage
			$content = strip_tags(Markdown::defaultTransform($billet->body));
			$html .= '<h2 class="content-subhead"><a href="' . $this->httpRequest->getRootUri() . '/blog/billet?id=' . $billet->id . '">'.$billet->titre.'</a></h2>';
			$html .= '<span class="date">'.$this->generateDate($billet->updated_at).'</span><p>';
            $html .= strlen($content) > VueBlog::SIZE_BODY ? substr($content, 0, VueBlog::SIZE_BODY).' ...' :$content ;
            $html .= '</p>';
            $html .= '</div>';
		}
		
		return $html;
	}

	private function generateCatsBlock() {
		$rootUri = $this->httpRequest->getRootUri();
		$html = <<<EOD
    		<div id="menu">
        		<div class="pure-menu pure-menu-open">
            	<a class="pure-menu-heading" href="#">Categorie</a>
EOD;

		$all = Categorie::all();

		$html .= '<ul>';
		foreach($all as $cat) {
			$html .= '<li ';
			if(($this->cat != NULL) && ($cat->id == $this->cat->id)) {
				$html .= 'class="menu-item-divided pure-menu-selected"';
			}
			$html .= '><a href="';
			$html .= $rootUri . '/blog/cat?id=' . $cat->id;
			$html .= '"> '. ucfirst($cat->titre) . '</a></li>';
		}
		$html .= '</ul></div></div>';

		return $html;
	}


	/**
	 * Genere la liste des categories.
	 * @param $categories Ensemble des categories.
	 * @return Le contenu HTML associé.
	 */
	private function generateManyCat($Categories) {
		$html = '';
		/** Pour chaque billet */
		foreach ($Categories as $key => $cat) {
			$html .= '<div class="categorie">';
			// Enleve le balisage
			$html .= '<h2 class="content-subhead"><a href="' . $this->httpRequest->getRootUri() . '/blog/cat?id=' . $cat->id . '">'.$cat->titre.'</a></h2><p>';
            $html .= strlen($cat->description) > VueBlog::SIZE_BODY ? substr($cat->description, 0, VueBlog::SIZE_BODY).' ...' :$cat->description ;
            $html .= '</p>';
            $html .= '</div>';
		}
		
		return $html;
	}

	function generateLastTicketsBlock() {
		$rootUri = $this->httpRequest->getRootUri();
		$last = Billet::orderBy('created_at','DESC')->take(10)->get();
		$html = <<<EOD
    		<div id="menuBillets">
        		<div class="pure-menu pure-menu-open">
            	<a class="pure-menu-heading" href="#">Derniers billets</a>
EOD;
		$html .= '<ul>';

		$get = $this->httpRequest->get();
		foreach($last as $billet) {
			$html .= '<li ';
			if((strstr($this->httpRequest->getPathInfo(), 'billet') !== FALSE) && ($billet->id == $get['id'])) {
				$html .= 'class="menu-item-divided pure-menu-selected"';
			}
			$html .= '><a href="' . $rootUri . '/blog/billet?id=' . $billet->id . '">';
			$html .= $billet->titre . '</a></li>';
		}
		$html .= '</ul></div></div>';
		return $html;
	}

	/**
	 * Genere un date formatée.
	 * @param date Chaine de carectere sous forme d'une date.
	 */
	function generateDate($date) {
		$date = new \DateTime($date);
		return $date->format('d-m-Y');
	}


}