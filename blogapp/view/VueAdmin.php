<?php
namespace blogapp\view;

use \blogapp\model\Categorie;
use \blogapp\model\Billet;
use \generic\View;
use \generic\utils\HttpRequest;

/**
 * Vue admin.
 * Cette vue permet d'afficher les formulaires
 * pour ajouter un billet ou une catégorie.
 *
 * @author Renan Strauss (S3A)
 * @author Yann Prono (S3A)
 */
class VueAdmin extends \generic\View {

	private $categories;
	
	const BILLET  = 0;
	const CAT = 1;
	const USER_SAVE = 2;

	public function __construct($req, $categories = null) {
		parent::__construct($req);
		$this->categories = $categories;
	}

	public function render($type) {
		$html = $this->header();
		$html .= $this->navbar();

		switch ($type) {
			case self::BILLET : 
				$content = $this->generateFormBillet();
				$title = "Ajouter un nouveau billet";
				break;
			case self::CAT : 
				$content = $this->generateFormCat();
				$title = "Ajouter une nouvelle categorie";
				break;			
			case self::USER_SAVE : 
				$content = $this->generateFormUser();
				$title = "S'enregistrer";
				break;			
			default:
				$content = "";
				$title = "";
		}

		$html .= <<<EOD
		<div id="main">
		<div class="header">
		<h1>$title</h1>
		</div>

		<div class="content">
		$content
		</div>
		</div>
EOD;
		$html .= $this->footer();
		echo $html;
	}

	private function generateFormBillet() {
		$html ='<div class="pure-u-1-1">';
		if(isset($_SESSION['info'])) {
			$html .= '<p class="pure-flash pure-flash-warning">'.$_SESSION['info'].'</p>';
			unset($_SESSION['info']);
		}
		$html .= '<form class="pure-form pure-form-aligned" enctype="multipart/form-data" method="POST" action="'.$this->request->getRootUri().'/admin/billet/save">';
		$html .= <<<EOD
		<fieldset>
		<div class="pure-control-group">
		<label for="title">Titre</label>
		<input id="title" name="titre" type="text" placeholder="Titre du billet">
		</div>

		<div class="pure-control-group">
		<label for="body">Contenu</label>
EOD;
		$html .= '<textarea id="form_billet_body" name="body" cols="30" rows="5" oninput="this.editor.update()" placeholder="Entre le contenu du billet ...">';
		$html .= !empty($_SESSION['body']) ? $_SESSION['body'] : '';
		unset($_SESSION['body']);
		$html .= '</textarea></div>';
		$html .= <<<EOD
		
		<div class="pure-control-group">
		<label for="category">Categorie</label>
		<select name="cat_id" id="category">
EOD;
		foreach ($this->categories as $key => $value) {        	
			$html .= '<option name="cat_id" '.'value="'.$key.'">'.$value.'</option>';
		}
		$html .= '</select></div>';
		$html .= <<<EOD
		<div class="pure-control-group">
			<label for="image">Image (Optionnelle)</label>
			<input type="file" name="image"/>
		</div>
EOD;
		
		$html .='<div class="pure-controls"><button type="submit" class="pure-button pure-button-primary">Sauvegarder</button></div></fieldset></form></div>';
		$html .= '<p>Prévisualisation (markdown) :</p>';
		$html .= '<div id="form_billet_preview"></div>';
		return $html;
	}

	private function generateFormCat() {
		$html ="";
		if(isset($_SESSION['info'])) {
			$html .= '<p class="pure-flash pure-flash-warning">'.$_SESSION['info'].'</p>';
			unset($_SESSION['info']);
		}
		$html .= '<form class="pure-form pure-form-aligned" method="POST" action="'.$this->request->getRootUri().'/admin/cat/save">';
		$html .= <<<EOD
		<fieldset>
			<div class="pure-control-group">
				<label for="name_cat">Nom</label>
				<input id="name_cat" name="titre" type="text" placeholder="Nom de la catégorie">
			</div>

			<div class="pure-control-group">
				<label for="description">Description</label>
				<textarea id="description" name="description"  placeholder="Entre la description ..."></textarea>
			</div>
EOD;
		$html .='<div class="pure-controls"><button type="submit" class="pure-button pure-button-primary">Ajouter</button></div></fieldset></form>';
		return $html;
	}

}