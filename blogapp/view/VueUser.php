<?php
namespace blogapp\view;

use \blogapp\model\Categorie;
use \blogapp\model\Billet;
use \generic\View;
use \generic\utils\HttpRequest;

class VueUser extends \generic\View {

	const LOGIN = 1;
	const REGISTER = 2;

	public function __construct($req) {
		parent::__construct($req);
	}

	public function render($type) {
		$html = $this->header();
		$html .= $this->navbar();

		$title = ($type == self::REGISTER) ? 'S\'enregistrer' : ($type == self::LOGIN ? 'Se connecter' : 'Erreur');
		$content = $this->generateFormUser($type);

		$html .= <<<EOD
		<div id="main">
		<div class="header">
		<h1>$title</h1>
		</div>

		<div class="content">
		$content
		</div>
		</div>
EOD;
		$html .= $this->footer();
		echo $html;
	}


	private function generateFormUser($type) {
		$html ="";
		if(isset($_SESSION['info'])) {
			$html .= '<p class="pure-flash pure-flash-warning">'.$_SESSION['info'].'</p>';
			unset($_SESSION['info']);
		}

		$html .= '<form class="pure-form pure-form-aligned" method="POST" action="'.$this->request->getRootUri().'/user/';
		$html .= ($type == self::REGISTER) ? 'save' : ($type == self::LOGIN ? 'login' : '');
		$html .= '">';
		$html .= <<<EOD
		<fieldset>
			<div class="pure-control-group">
				<label for="username">Nom d'utilisateur</label>
				<input  id="username" name="username" type="text" placeholder="Username">
			</div>
			<div class="pure-control-group">
				<label for="password">Mot de passe</label>
				<input id="password" name="password" type="password" placeholder="*******">
			</div>
EOD;
		if($type == self::REGISTER) {
			$html .= <<<EOD
			<div class="pure-control-group">
			<label for="validator">Confirmer le mot de passe</label>
			<input id="validator" name="password_validator" type="password">
			</div>	
EOD;
		}
		
		$html .= '<div class="pure-controls"><button type="submit" class="pure-button pure-button-primary">';
		$html .= ($type == self::REGISTER) ? 'S\'enregistrer' : ($type == self::LOGIN ? 'Se connecter' : 'Erreur');
		$html .= '</button></div></fieldset></form>';

		return $html;
	}
}