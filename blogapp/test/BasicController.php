<?php
namespace blogapp\test;

use \generic;

class BasicController extends \generic\Controller {
	public function __construct($req) {
		parent::__construct($req);
	}

	public function dummyAction() {
		$str = "dummyAction";
		$get = $this->httpRequest->get();

		if(isset($get['id'])) {
			$str = $str . " " . $get['id'];
		}

		print $str;
	}
}