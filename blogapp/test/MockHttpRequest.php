<?php
namespace blogapp\test;

use \generic\utils;

class MockHttpRequest extends \generic\utils\HttpRequest {
	public function __construct($method, $scriptName, $reqUri, $query, $get, $post) {
		$this->method = $method;
		$this->scriptName = $scriptName;
		$this->requestUri = $reqUri;
		$this->query = $query;
		$this->get = $get;
		$this->post = $post;
	}
}