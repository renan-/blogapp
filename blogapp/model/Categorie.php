<?php

namespace blogapp\model;

/**
 * Modele Categorie
 * Une catégorie est composé d'un titre 
 * et d'une description.
 *
 * @author Renan Strauss (S3A)
 * @author Yann Prono 	(S3A)
 */
class Categorie extends \Illuminate\Database\Eloquent\Model {

	/**
	 * @return les IDs des billets ayant cette catégorie.
	 */
	public function billets() {
		return $this->hasMany('blogapp\model\Billet', 'cat_id', 'id')->orderBy('updated_at','DESC');
	}

}