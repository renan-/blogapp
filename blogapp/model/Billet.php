<?php

namespace blogapp\model;

/**
 * Modele Billet permettant de communiquer avec la DB.
 * Un billet est composé d'un titre et d'un contenu.
 * Un billet appartient à une catégorie.
 * Un billet est écrit par un utilisateur unique.
 * 
 * @author Renan Strauss (S3A)
 * @author Yann Prono 	(S3A)
 */
class Billet extends \Illuminate\Database\Eloquent\Model {

	/**
	 * @return l'ID de la catégorie du billet.
	 */
	public function categorie() {
		return $this->hasOne('blogapp\model\Categorie', 'id', 'cat_id');
	}

	public function image() {
		return $this->hasOne('blogapp\model\Image', 'id', 'image_id');
	}

	/**
	 * @return l'ID de l'utilisateur.
	 * Si l'utilisateur est inconnu, l'ID = -1.
	 */
	public function user() {
		return $this->hasOne('blogapp\model\User', 'id', 'user_id');
	}

}