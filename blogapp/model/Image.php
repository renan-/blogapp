<?php

namespace blogapp\model;

/**
 * Modele Image
 *
 * @author Yann Prono 	(S3A)
 * @author Renan Strauss (S3A)
 */ 
class Image extends \Illuminate\Database\Eloquent\Model {

	public $timestamps = false;
	
}