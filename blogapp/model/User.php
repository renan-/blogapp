<?php

namespace blogapp\model;

/**
 * Modele User
 * Un utilisateur est composé d'un nom d'utilisateur et d'un mot de passe.
 * Un utilisateur permet d'identifier un billet.
 *
 * @author Yann Prono 	(S3A)
 * @author Renan Strauss (S3A)
 */ 
class User extends \Illuminate\Database\Eloquent\Model {

	public $timestamps = false;
	
	/**
	 * @return Les IDs des billets de cet utilisateur 
	 */
	public function billets() {
		return $this->hasMany('blogapp\model\Billet', 'user_id', 'id')->orderBy('updated_at','DESC');
	}
}