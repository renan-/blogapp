<?php
namespace blogapp\control;

use \generic;
use \blogapp\view\VueUser;
use \blogapp\model\User;
use \generic\utils\Authentication;

/**
 * Controller gerant l'ajout
 * la connexion d'un utilisateur.
 *
 * @author Renan Strauss (S3A)
 * @author Yann Prono (S3A)
 */
class UserController extends \generic\Controller {
	
	public function __construct($req) {
		parent::__construct($req);
		$this->post = $this->httpRequest->getPost();
	}

	/**
	* Enregistre l'utilisateur dans la DB du blog.
	*/
	public function ajouterUtilisateur() {
		$vue = new VueUser($this->httpRequest);
		$vue->render(VueUser::REGISTER);
	}

	public function sauverUtilisateur() {
		if(!empty($this->post['username']) && !empty($this->post['password']) && !empty($this->post['password_validator'])) {
			$username = filter_var($this->post['username'], FILTER_SANITIZE_STRING);
			$password = filter_var($this->post['password'], FILTER_SANITIZE_STRING);
			$password_validator = filter_var($this->post['password_validator'], FILTER_SANITIZE_STRING);

			$errors = $this->checkRegisterUser($username, $password, $password_validator);
			if(empty($errors)) {
				Authentication::createUser($username, $password);
				$_SESSION['info'] = "Enregistrement effectué avec succès !";
				header('location: '.$this->httpRequest->getRootUri());
			}
			else {
				$_SESSION['info'] = $errors[0];
				header('location: '.$this->httpRequest->getRootUri().'/user/add');
			}
		}
		else {
			$_SESSION['info'] = "Veuillez Renseigner tous les champs !";	
			header('location: '.$this->httpRequest->getRootUri().'/user/add');
		}
	}

	/**
	 * Verifie la validité des données pour enregistrer un nouvel user.
	 * @param username Le nom d'utilisateur.
	 * @param password Le mot de passe.
	 * @param password_validator Le confirmation du mot de passe.
	 * @return Un tableau contenant les messages d'erreurs.
	 */
	private function checkRegisterUser($username, $password, $password_validator ) {
		$errors = array();
		$exist = User::where('username', '=', $username)->first();
		if($exist == null) {

			if($password != $password_validator){
				$errors[0] = "Mots de passe différents";
			}
		}
		else {
			$errors[0] = "Nom d'utilisateur déja utilisé";
		}
		return $errors;
	}


	public function connexionUtilisateur() {
		$vue = new VueUser($this->httpRequest);
		$vue->render(VueUser::LOGIN);
	}

	public function connecterUtilisateur() {
		if(!empty($this->post['username']) && !empty($this->post['password'])) {
			$username = filter_var($this->post['username'], FILTER_SANITIZE_STRING);
			$password = filter_var($this->post['password'], FILTER_SANITIZE_STRING);
			if(Authentication::authenticate($username, $password)) {
				$_SESSION['info'] = "Connexion effectuée avec succès !";
				header('location: '.$this->httpRequest->getRootUri());
			} else {
				$_SESSION['info'] = "Erreur lors de la connexion";
				header('location: '.$this->httpRequest->getRootUri().'/user/signin');
			}
		}
		else {
			$_SESSION['info'] = "Veuillez Renseigner tous les champs !";	
			header('location: '.$this->httpRequest->getRootUri().'/user/signin');
		}
	}

	public function deconnecterUtilisateur() {
		Authentication::logout();

		$_SESSION['info'] = "Déconnexion effectuée avec succès !";
		header('location: '.$this->httpRequest->getRootUri());
	}
}