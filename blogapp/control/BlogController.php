<?php
namespace blogapp\control;

use \generic;
use \blogapp\model\Billet;
use \blogapp\model\Categorie;
use \blogapp\model\User;
use \blogapp\view\VueBlog;

class BlogController extends \generic\Controller {
	public function __construct($req) {
		parent::__construct($req);
	}

	/**
	 * Récupere tous les billets, les derniers en premier.
	 */
	public function listerBillets()  {
		$billets = Billet::orderBy('created_at', 'DESC')->get();
		$get = $this->httpRequest->get();

		$vue = new VueBlog($this->httpRequest, NULL, $billets);
		$vue->render("Tous les billets", VueBlog::LISTE, false);
	}

	public function listerBilletsAuteur()  {
		$billets = Billet::orderBy('created_at', 'DESC')->get();
		$get = $this->httpRequest->get();
		$billets = Billet::where('user_id','=',$get['id'])->get();
		$vue = new VueBlog($this->httpRequest, NULL, $billets);
		$vue->render("Billets postés par ".User::find($get['id'])->username, VueBlog::LISTE, false);
	}

	public function index()  {
		$billets = Billet::orderBy('created_at', 'DESC')->get();
		$get = $this->httpRequest->get();

		$vue = new VueBlog($this->httpRequest, NULL, $billets);
		$vue->render("En actualité", VueBlog::LISTE, false);
	}

	/**
	* Affiche en détail un billet passé en GET.
	*/
	public function afficherBillet() {
		$get = $this->httpRequest->get();
		$billet = Billet::find(filter_var($get['id'], FILTER_SANITIZE_NUMBER_INT));

		$vue = new VueBlog($this->httpRequest, $billet->categorie, array($billet));
		$vue->render($billet->titre, VueBlog::DETAIL, false);
	}

	public function messagesCategorie() {
		$get = $this->httpRequest->get();
		$cat = Categorie::find(filter_var($get['id'], FILTER_SANITIZE_NUMBER_INT));
		$vue = new VueBlog($this->httpRequest, $cat, $cat->billets);
		$vue->render($cat->titre, VueBlog::LISTE);
	}

	public function listerCategories() {
		$categories = Categorie::all();
		$vue = new VueBlog($this->httpRequest, NULL, $categories);
		$vue->render("Toutes les catégories", VueBlog::LISTE_CAT, false);

	}
} 