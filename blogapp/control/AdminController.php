<?php
namespace blogapp\control;

use \blogapp\model\Billet;
use \blogapp\model\Categorie;
use \blogapp\model\Image;
use \blogapp\view\VueAdmin;
use \Michelf\Markdown;
use \generic\utils\Authentication;

/**
 * Ce controller permet de gerer l'ajout de billets
 * et de catégories.
 *
 * @author Renan Strauss (S3A)
 * @author Yann Prono 	(S3A)
 */
class AdminController extends \generic\Controller {

	private $extensions = array( 'jpg' , 'jpeg' , 'gif' , 'png' );

	public function __construct($req) {
		parent::__construct($req);
		if(Authentication::isGuest()) {
			header('location: '.$this->httpRequest->getRootUri().'/user/signin');
			die();
		}
		
		$this->post = $this->httpRequest->getPost();
	}

	/**
	 * Permet d'afficher la vue approprié pour l'ajout d'un billet.
	 */
	public function ajouterMessage() {
		$cats = Categorie::all();
		$assoc = array();
		foreach($cats as $cat) {
			$assoc[$cat->id] = $cat->titre;
		}

		$vue = new VueAdmin($this->httpRequest,$assoc);
		$vue->render(VueAdmin::BILLET);
	}

	/**
	 * Permet de sauvegarder le billet dans la DB.
	 */
	public function sauverMessage() {
		if(!empty($this->post['titre'])) {
			$titre = filter_var($this->post['titre'], FILTER_SANITIZE_STRING);
			$body = filter_var($this->post['body'], FILTER_SANITIZE_STRING);
			$cat_id = filter_var($this->post['cat_id'], FILTER_SANITIZE_NUMBER_INT);
			$img_id = $this->checkImage();
			$billet = new Billet();
			$billet->titre = $titre;
			//$billet->body = Markdown::defaultTransform($body);
			$billet->body = $body;
			$billet->cat_id = $cat_id;
			$billet->image_id = $img_id;
			$billet->user_id = Authentication::getUserId();

			$billet->save();
			$_SESSION['info'] = "Billet sauvegardé avec succès !";
			header('location: '.$this->httpRequest->getRootUri());
		}
		else {
			$_SESSION['info'] = "Veuillez Renseigner au moins un titre !";	
			$_SESSION['body'] = $this->post['body'];
			header('location: '.$this->httpRequest->getRootUri().'/admin/billet/add');
		}
	}

	/**
	 * Affiche la vue pour l'ajout d'une catégorie.
	 */
	public function ajouterCategorie() {
		$vue = new VueAdmin($this->httpRequest);
		$vue->render(VueAdmin::CAT);
	}

	/**
	 * Sauvegarde la catégorie dans la DB.
	 */
	public function sauverCategorie() {
		if(!empty($this->post['titre']) && !empty($this->post['description'])) {
			$titre = filter_var($this->post['titre'], FILTER_SANITIZE_STRING);
			$desc = filter_var($this->post['description'], FILTER_SANITIZE_STRING);
			$exist = Categorie::where('titre','=',$this->post['titre'])->get()->first();
			if($exist == null) {
				$cat = new Categorie();
				$cat->titre = $titre;
				$cat->description = $desc;
				$cat->save();
				$_SESSION['info'] = "Catégorie ajoutée avec succès !";
				header('location: '.$this->httpRequest->getRootUri());
			}
			else {
				$_SESSION['info'] = "Catégorie déjà existante !";	
				header('location: '.$this->httpRequest->getRootUri().'/admin/cat/add');
			}

		}
		else {
			$_SESSION['info'] = "Veuillez Renseigner tous les champs !";	
			header('location: '.$this->httpRequest->getRootUri().'/admin/cat/add');
		}
	}

	public function checkImage() {
		$id = -1;
		if($_FILES['image']['error'] == 0) {
			$maxwidth = 1000;
			$maxheight = 800;
			$ext = strtolower(substr(strrchr($_FILES['image']['name'], '.') ,1));
			if (!in_array($ext,$this->extensions)) {
				$_SESSION['info'] = "Type de fichier non accepté";	
				header('location: '.$this->httpRequest->getRootUri().'/admin/billet/add'); 
				die();
			}
			$image_sizes = getimagesize($_FILES['image']['tmp_name']);
			if ($image_sizes[0] < $maxwidth && $image_sizes[1] < $maxheight) {
				$path = dirname(dirname(__DIR__)).'/public/img/';

				if(!file_exists($path)) {
					mkdir($path, 0777, true);
				}
				$filename = md5($_FILES['image']['tmp_name']);
				$filename .= '.'.$ext;
				$resultat = move_uploaded_file($_FILES['image']['tmp_name'],$path.$filename);
				$image = new Image();
				$image->filename = $filename;
				$image->save();
				$id = $image->id;
			}
			else {
				$_SESSION['info'] = "Image trop grande";	
				header('location: '.$this->httpRequest->getRootUri().'/admin/billet/add'); 
				die();
			}

		}
		return $id;
	}

}