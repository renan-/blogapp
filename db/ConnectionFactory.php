<?php
namespace db;

class ConnectionFactory {
	private static $connection = null;
	private static $config;

	/**
     * Les noms de clés
 	*/
	const DRIVER = 'driver';
	const HOST = 'host';
	const DBNAME = 'dbname';

	const USER = 'user';
	const PWD  = 'password';

	public static function setConfigFile($ini) {
		self::$config = parse_ini_file($ini);
	}

	public static function makeConnection() {
		if(self::$connection == null) {
			$dsn = sprintf("%s:host=%s;dbname=%s", 
					self::$config[self::DRIVER],
					self::$config[self::HOST],
					self::$config[self::DBNAME]
			);

			try {
				self::$connection = new \PDO($dsn, self::$config[self::USER], self::$config[self::PWD],
					array(
						\PDO::ATTR_PERSISTENT => true,
						\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
						\PDO::ATTR_EMULATE_PREPARES => false,
						\PDO::ATTR_STRINGIFY_FETCHES => false
					)
				);

				self::$connection->prepare('SET NAMES \'UTF8\'');
			} catch(\PDOException $e) {
				echo $e->getMessage();
				//throw new \DBException("Connection : $dsn " . $e->getMessage() . '<br />');
			}
		}

		return self::$connection;
	}
}