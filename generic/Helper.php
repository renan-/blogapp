<?php
namespace generic;

use \Illuminate\Database\Capsule\Manager as Capsule;

class Helper {
	public static function bootEloquent($conf) {
		$cap = new Capsule();
		$cap->addConnection(parse_ini_file($conf));
		$cap->setAsGlobal();
		$cap->bootEloquent();
	}
}