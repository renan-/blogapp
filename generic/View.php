<?php
namespace generic;

use \generic\utils\Authentication;
use \generic\utils\HttpRequest;

/**
 * Vue abstraite.
 * DRY
 *
 * @author Renan Strauss (S3A)
 * @author Yann Prono (S3A)
 */
abstract class View {

	protected $request;

	public function __construct($req) {
		$this->request = $req;
	}

	abstract public function render($type);

	/**
	 * @return Le code HTML du header.
	 */
	protected function header() {
		$html = "<!doctype html><html><head><title>Blog</title>";
		$html .= '<link rel="stylesheet" href="'.$this->request->getRootUri().'/public/css/pure-min.css">';
		$html .= '<link rel="stylesheet" href="'.$this->request->getRootUri().'/public/css/side-menu.css">';
		$html .= '<link rel="stylesheet" href="'.$this->request->getRootUri().'/public/css/custom.css">';
		$html .= '<meta name="viewport" content="width=device-width, initial-scale=1">';
		$html .= '<meta charset="utf-8">';
		$html .= "</head><body>";

		return $html;
	}

	protected function navbar() {
		$html = <<<EOD
        <div class="pure-menu pure-menu-open pure-menu-horizontal navbar">
            <ul>
EOD;
        $html .= '<li class="pure-menu-selected"><a href="'.$this->request->getRootUri().'">Accueil</a></li>';
        if(Authentication::isGuest()) {
        	$html .= '<li><a href="'.$this->request->getRootUri().'/user/add'.'">S\'enregistrer</a></li>';
        	$html .= '<li><a href="'.$this->request->getRootUri().'/user/signin'.'">Se connecter</a></li>';
        } else {
		$html .= '<li><a href="'.$this->request->getRootUri().'/admin/billet/add'.'">Ajouter un billet</a></li>';
        $html .= '<li><a href="'.$this->request->getRootUri().'/admin/cat/add'.'">Ajouter une catégorie</a></li>';
       	$html .= '<li><a href="'.$this->request->getRootUri().'/blog/user?id='.$_SESSION['profile']['uid'].'">'.$_SESSION['profile']['username'].'</a></li>';
		$html .= '<li><a href="'.$this->request->getRootUri().'/user/logout'.'">Se déconnecter</a></li>';
        }
        $html .= '</ul></div>';

        return $html;
	}

	/**
	 * @return Le code HTML du footer.
	 */
	protected function footer() {
		$html = '<script src="'.$this->request->getRootUri().'/public/js/ui.js"></script>';
		$html .= '<script src="'.$this->request->getRootUri().'/public/js/markdown-js/markdown.js"></script>';
		$html .= <<<EOD
<script>
	function Editor(input, preview) {
		this.update = function () {
	    	preview.innerHTML = markdown.toHTML(input.value);
		};
		input.editor = this;
		this.update();
	}
	var $ = function (id) { return document.getElementById(id); };
	new Editor($("form_billet_body"), $("form_billet_preview"));
</script>
EOD;
		$html .= '</body></html>';

		return $html;
	}

}