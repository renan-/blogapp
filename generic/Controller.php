<?php
namespace generic;

abstract class Controller {
	protected $httpRequest;

	public function __construct($req) {
		$this->httpRequest = $req;
	}
}