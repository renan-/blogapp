<?php
namespace generic;

abstract class Model {
	protected $attributes;
	protected static $primaryKey = 'id';

	public function __construct() {
		$this->attributes = array();
	}

	public function delete() {
		$sql = 'DELETE FROM ' . static::determineTableName($this) . ' WHERE ' . static::$primaryKey . ' = ?;';

		$db = \db\ConnectionFactory::makeConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam(1, $this->attributes[static::$primaryKey], \PDO::PARAM_INT);

		$stmt->execute();
	}

	public function insert() {
		$cols = array_keys($this->attributes);
		/**
		 * Si l'id a ete defini, on le supprime
		 * (auto increment)
		*/
		if(array_key_exists(static::$primaryKey, $cols)) {
			unset($cols[static::$primaryKey]);
		}

		$vals = array();

		for($i = 0; $i < count($cols); $i++) {
			$vals[] = '?';
		}

		$colsReq = implode(',', $cols);
		$valsReq = implode(',', $vals);

		$sql = 'INSERT INTO ' . static::determineTableName($this) . ' (' . $colsReq . ') ';
		$sql .= 'VALUES (' . $valsReq . ');';

		$db = \db\ConnectionFactory::makeConnection();
		$stmt = $db->prepare($sql);

		$stmt->execute(array_values($this->attributes));

		$pk = static::$primaryKey;
		$this->$pk = $db->lastInsertId(static::determineTableName($this));
	}

	public function update() {
		$cols = array_keys($this->attributes);
		$vals = array();

		for($i = 0; $i < count($cols); $i++) {
			$vals[] = '?';
		}

		$colsReq = implode(',', $cols);
		$valsReq = implode(',', $vals);

		$sql = 'INSERT INTO ' . static::determineTableName($this) . ' (' . $colsReq . ') ';
		$sql .= 'VALUES (' . $valsReq . ');';

		$db = \db\ConnectionFactory::makeConnection();
		$stmt = $db->prepare($sql);

		$stmt->execute(array_values($this->attributes));
	}

	public static function findOne($id) {
		/* Premierement, on determine le nom de la table */
		$model = get_called_class();
		$tableName = static::determineTableName($model);

		// On construit ensuite la requete
		$sql = 'SELECT * FROM ' . $tableName . ' WHERE ' . static::$primaryKey . ' = ?;';
		$db = \db\ConnectionFactory::makeConnection();

		$stmt = $db->prepare($sql);
		$stmt->bindParam(1, $id, \PDO::PARAM_INT);
		$stmt->execute();

		$row = $stmt->fetch(\PDO::FETCH_ASSOC);
		/**
		 * Finalement, on "remplit"
		 * l'objet avec les informations de la bdd
		*/
		return Model::populate($model, $row);
	}

	public static function findMany($col = NULL, $val = NULL) {
		/* Premierement, on determine le nom de la table */
		$model = get_called_class();
		$tableName = static::determineTableName($model);

		$objs = array();
		$db = \db\ConnectionFactory::makeConnection();
		
		if(is_null($col) && is_null($val)) {
			$sql = 'SELECT * FROM ' . $tableName . ';';
			foreach($db->query($sql) as $row) {
				array_push($objs, Model::populate($model, $row));
			}
		} else {
			$sql = 'SELECT * FROM ' . $tableName . ' WHERE ' . $col . ' = ?;';
			$stmt = $db->prepare($sql);
			$stmt->bindParam(1, $val);
			$stmt->execute();

			while($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
				array_push($objs, Model::populate($model, $row));
			}
		}

		return $objs;
	}

	/**
	 * By default, the name of the model (in lower-case)
	 * is considered as the table name, but it's possible
	 * to change this behavior by overriding this method
	*/
	protected static function determineTableName($model) {
		$reflect = new \ReflectionClass($model);
		$tableName = lcfirst($reflect->getShortName());

		return $tableName;
	}

	private static function populate($model, $assoc) {
		$one = new $model();
		foreach ($assoc as $key => $value) {
            $one->$key = $value;
        }

        return $one;
	}

	public function __get($name) {
		if(array_key_exists($name, $this->attributes)) {
			return $this->attributes[$name];
		}

		return null;
	}

	public function __set($name, $value) {
		$this->attributes[$name] = $value;
	}
}