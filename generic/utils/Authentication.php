<?php

namespace generic\utils;
use \blogapp\model\User;

class Authentication {

	const PROFILE_KEY = 'profile';

	public static function createUser($userName, $password) {
		$user = new User;
		$user->username = $userName;
		$user->password = password_hash($password, PASSWORD_DEFAULT, array('cost'=> 12));
		$user->save();
	}

	public static function authenticate($username, $password) {
		$user = User::where('username', '=', $username)->first();
		if(password_verify($password, $user->password)) {
			Authentication::loadProfile($user->id);

			return true;
		}

		return false;
	}

	private static function loadProfile($uid) {
		unset($_SESSION[Authentication::PROFILE_KEY]);

		$user = User::find($uid);
		$_SESSION[Authentication::PROFILE_KEY] = array(
			'uid' => $uid,
			'username' => $user->username,
			'client-ip' => filter_var($_SERVER['REMOTE_ADDR']?:($_SERVER['HTTP_X_FORWARDED_FOR']?:$_SERVER['HTTP_CLIENT_IP']), FILTER_VALIDATE_IP)
		);
	}

	public static function logout() {
		unset($_SESSION[Authentication::PROFILE_KEY]);
	}

	public static function isGuest() {
		return !isset($_SESSION[Authentication::PROFILE_KEY]);
	}

	public static function getUserId() {
		return self::isGuest() ? -1 : $_SESSION[Authentication::PROFILE_KEY]['uid'];			
	}

	public static function checkAccessRights($required) {

	}
}