<?php
namespace generic\utils;

class HttpRequest {
	/**
	 * Whether it's GET, POST...
	*/
	protected $method;

	protected $scriptName;
	protected $requestUri;
	protected $query;
	protected $get;
	protected $post;

	public function __construct() {
		$this->method = $_SERVER['REQUEST_METHOD'];
		$this->scriptName = $_SERVER['SCRIPT_NAME'];
		$this->requestUri = $_SERVER['REQUEST_URI'];
		$this->query = $_SERVER['QUERY_STRING'];
		$this->get = $_GET;
		$this->post = $_POST;
	}

	public function getMethod() {
		return $this->method;
	}

	public function getScriptName() {
		return $this->scriptName;
	}

	public function getRequestUri() {
		return $this->requestUri;
	}

	public function getQuery() {
		return $this->query;
	}

	public function get() {
		return $this->get;
	}

	public function getPost() {
		return $this->post;
	}

	public function getPathInfo() {
		return str_replace(dirname($this->getScriptName()), '', parse_url($this->getRequestUri(), PHP_URL_PATH));
	}

	public function getRootUri() {
		return dirname($_SERVER['SCRIPT_NAME']);
	}
}