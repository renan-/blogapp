<?php
namespace generic\dispatch;

class Dispatcher {
	private $httpRequest;

	private $routes;
  
	public function __construct($req) {
		$this->httpRequest = $req;

		$this->routes = array('GET' => array(), 'POST' => array());
	}

	public function addRouteGet($url, $clazz, $meth) {
		$this->addRoute('GET', $url, $clazz, $meth);
	}

	public function addRoutePost($url, $clazz, $meth) {
		$this->addRoute('POST', $url, $clazz, $meth);
	}

	public function dispatch() {
		foreach($this->routes[$this->httpRequest->getMethod()] as $url => $route) {
			if($url == $this->httpRequest->getPathInfo()) {
				$ctrl = new $route['clazz']($this->httpRequest);
				$ctrl->$route['meth']();
			}
		}
	}

	public function addRoute($type, $url, $clazz, $meth) {
		$this->routes[$type][$url] = array('clazz' => $clazz, 'meth' => $meth);
	}
}